#!/usr/bin/env python

import sys
import getopt
import csv
import re
from pathlib import Path

def table1(csv_file):
    with csv_file.open() as f:
        csv_reader = zip(*csv.reader(f, delimiter=',', quotechar='"'))
        objs = list(map(lambda i: i.group(1), filter(None, map(lambda i: re.search('(\d+\.\d+\.\d+) .+', i), next(csv_reader)))))
        print("\\begin{{tabular}}{{l {}| r}}".format("| c "*(len(objs))))
        print("&{}&\\\\".format("&".join(map(lambda i: '\\thead{{{}}}'.format(i),objs))))
        print("\\hline")

        for row in csv_reader:
            if len(row[0]) > 0:
                header = '\\rhead{{{}}}'.format(row[0])
                values = map(lambda i: '$\\bullet$' if len(i) else ' ', row[1:])

                print("\\rhead{{{0}}}&{1}&\\rhead{{{0}}}\\\\".format(header, "&".join(values)))
                #print("\\hline")

        print("\\end{tabular}")

def list1(csv_file, hideEmpty=False):
    with csv_file.open() as f:
        csv_reader = zip(*csv.reader(f, delimiter=',', quotechar='"'))
        objs = list(map(lambda s: s[0:100] + '...' if len(s) > 100 else s, filter(None, next(csv_reader))))

        print("\\begin{longtabu} to \\textwidth {c | X[b]}")
        print("\\textbf{Lesson} & \\textbf{Objectives}\\\\")
        for row in csv_reader:
            if len(row[0]) > 0:
                header = row[0]
                values = row[1:]

                sigs = list(filter(lambda v: len(v[1]), enumerate(values)))
                if len(sigs) > 0:
                    print("\\hline")
                    for i,s in sigs:
                        print("{}&{}\\\\".format(header, objs[i]))
                        header = ''
                elif not hideEmpty:
                    print("\\hline")
                    print("{}& \\textit{{Assessment}}\\\\".format(header))

        print("\\end{longtabu}")

def main(argv):
    csv_loc = 'objectives.csv'
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        print("objectives.py -i <inputfile>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("objectives.py -i <inputfile>")
            sys.exit()
        elif opt in ("-i", "--ifile"):
            csv_loc = arg

    csv_file = Path(csv_loc)
    if not csv_file.is_file():
        print("Could not find CSV file: {}".format(csv_loc))

    sys.stdout = open('content-table.tex', 'w')
    table1(csv_file)

    sys.stdout = open('content-list-all.tex', 'w')
    list1(csv_file, False)

    sys.stdout = open('content-list-hidden.tex', 'w')
    list1(csv_file, True)

if __name__ == '__main__':
    main(sys.argv[1:])
